const si = require('systeminformation');
const { powerMonitor } = require("electron");
const { logger } = require("../log/log");
const sv = require("../service/activeWinLogService");
var timerLog = null;
var timerStop = null;
module.exports.run = async () => {
	try {
		global.Title = null;
		logger.info("start run app");
		var systemInformation = await si.system();
		if (timerLog == null) {
			timerLog = setInterval(() => {
				sv.CallActiveWinLog(systemInformation.uuid);
			}, 1000);
		};
		if (timerStop == null) {
			timerStop = setInterval(() => {
				//  console.log(powerMonitor.getSystemIdleTime());
				var status = powerMonitor.getSystemIdleState(20);
				if (status == "idle" || status == "locked") {
					clearInterval(timerLog);
					clearInterval(timerStop);
					timerStop = null;
					timerLog = null;
				}

			}, 1000);
		};
	} catch (error) {
		logger.error(error.message);
	}
}