const ev = require("../event/active-win");
const activeWin = require("../index");
const repo = require("../repository/sqllight/repository");

module.exports.CallActiveWinLog = (uuid) => {
	ev.activeWinEmit(uuid);
}

module.exports.HandleActiveWinLog = async (uuid) => {
	var av = await activeWin();
	if (av) {
		if (global.Title != av.title) {
			global.Title = av.title;
			await repo.WinLogRepository(av, uuid);
		}

	}
}
