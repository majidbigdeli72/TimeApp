var sqlite3 = require('sqlite3').verbose();
var tbl = require("../const/sqllight");
const { Database } = require('sqlite3');
const logger = require("../log/log").logger;

module.exports.db = async () => {
	try {
		return new sqlite3.Database(tbl.DbSource);
	} catch (error) {
		logger.error(error.message);
		throw error;
	}
}

module.exports.createTabels = async () => {
	try {
		let db = await this.db();
		await db.run(tbl.createTabel);
	} catch (error) {
		logger.error(error.message);
		throw error;
	}
}