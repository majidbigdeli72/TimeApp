const winston = require("winston");
require('winston-daily-rotate-file');

module.exports.logger = winston.createLogger({
	transports: [
	  new (winston.transports.DailyRotateFile)({
		filename: './logs/application-%DATE%.log',
		datePattern: 'YYYY-MM-DD',
		prepend: true,
		colorize: true,
		json: false, //Setting JSON as false
		zippedArchive: false,
		timestamp: function () {
		  return Date.now();
		},
		formatter: function (options) {
		  return options.timestamp() + '-' + process.env.NODE_ENV + '-message:' + (options.message ? options.message : '')
		}
	  })]
  });

//   if (process.env.NODE_ENV !== 'production') {
// 	logger.add(new winston.transports.Console({
// 	  format: winston.format.simple()
// 	}));
//   }