const database = require("../../database/sqllight");

module.exports.WinLogRepository = async (activeWin, uuid) => {
	var insert = 'INSERT INTO event_log (systemId,platform,title,name,url,duration) VALUES (?,?,?,?,?,?)';
	let db = await database.db();
	console.log(activeWin);
	await db.run(insert, [uuid, activeWin.platform, activeWin.title,activeWin.owner.name,activeWin.url,5]);
	db.close();
}