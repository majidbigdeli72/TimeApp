
module.exports.createTabel = `CREATE TABLE IF NOT EXISTS event_log (
		systemId TEXT NOT NULL,
		platform TEXT NULL,
		title TEXT NULL,
		name TEXT NULL,
		url TEXT NULL,
		duration INTEGER NULL
	  );`;
module.exports.DbSource = "timeApp.sqlite3"
module.exports.logFile = './logs';


