const EventEmitter = require('events');
const emitter = new EventEmitter();
const sv = require("../service/activeWinLogService");


module.exports.activeWinEmit = (uuid) => {
  emitter.emit('event', uuid);
};

emitter.on('event',(uuid) => {
 setImmediate(async () => {
    await sv.HandleActiveWinLog(uuid);
  });
});




