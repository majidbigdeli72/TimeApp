// src/views/UserList.js
var m = require("mithril");
require("../../content/css/login.css");
export default function Login() {
  function changeRoute() {
    m.route.set("/task");
  }
  return {
    view: () => (
      <div class="wrapper">
        <div class="container">
          <h1>خوش آمدید</h1>
          <form class="form">
            <input type="text" placeholder="نام کاربری" />
            <input type="password" placeholder="رمز عبور" />
            <button type="button" id="login-button" onclick={changeRoute}>
              ورود
            </button>
          </form>
        </div>
        <ul class="bg-bubbles">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    ),
  };
}
