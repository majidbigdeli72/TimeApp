// src/views/UserList.js
var m = require("mithril");
import "../../content/css/task.css";
export default function Task() {
  return {
    view: () => (
      <div style="background:#fff;border-radius: 8px;padding:8px;height:calc(100vh - 160px);min-height:300px;box-shadow: 0px 6px 12px #00000052;">
        <section>
          <div id="intro">
            <div
              class="scrollbar"
              style="flex:7;border-right:1px solid rgba(204, 204, 204, 0.59);height: calc(100vh - 180px);padding:15px"
            >
              <ul class="accourdian">
                <li class="lih">
                  <input id="ckk" type="checkbox" />
                  <i></i>
                  <h4 style="font-weight:600;color:rgb(173, 173, 173);margin-bottom: 12px;">
                    پیام گستر
                  </h4>
                  <div style="position: relative;overflow: hidden;transform: translate(0, 0);z-index: 2;">
                    <ul>
                      <li class="task_item">
                        <p>وظیفه ها</p>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
            <div
              class="scrollbar"
              style="flex:15;padding:15px;height: calc(100vh - 180px);padding:15px"
            >
              <ul style="direction: rtl;">
                <li class="task_item3" style="padding-left:20px;background:#dcdde1">
                  <span style="color:#27c26c;font-weight:600">وضیفه شماره 1</span>
                  <span class="badge" style="float:left">00:57:17</span>
                  <span class="badge" style="float:left;background:#ccc;border-radius:10px">پیام گستر</span>

                </li>
                <li class="task_item3">وضیفه شماره 2</li>
                <li class="task_item3">وضیفه شماره 3</li>
                <li class="task_item3">وضیفه شماره 4</li>
                <li class="task_item3">وضیفه شماره 5</li>
                <li class="task_item3">وضیفه شماره 6</li>
                <li class="task_item3">وضیفه شماره 7</li>
                <li class="task_item3">وضیفه شماره 8</li>
                <li class="task_item3">وضیفه شماره 9</li>
                <li class="task_item3">وضیفه شماره 10</li>
                <li class="task_item3">وضیفه شماره 11</li>
                <li class="task_item3">وضیفه شماره 12</li>

              </ul>
            </div>
          </div>
        </section>
      </div>
    ),
  };
}
