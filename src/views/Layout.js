var m = require("mithril");
import "../../content/css/layout.css";
//const ipc = require('electron').ipcRenderer;

export default function Layout(params) {
  let isRun = false;
  let timer = "00:00:00";
  console.log(process.env.NODE_ENV);

  // ipc.on("async-message-replay", (event, arg) => {
  //   const message = `Message reply: ${arg}`;
  // });

  let toggleStart = () => {
    // ipc.send("run", !isRun);
    isRun = !isRun;
  };

  return {
    view: (vnode) => (
      <div style="height:100vh">
        <aside>
          <nav class="nav-vertical">
            <div class="hamburger menu--active">
              <img
                src="./content/image/26275762.jfif"
                alt="Avatar"
                style="width:3.25em;border-radius: 50%;"
              />
            </div>
            <ul class="nav-list list__active">
              <li class="nav__item">
                <a href="#intro" class="nav__link c_red">
                  <i class="fa fa-home" aria-hidden="true"></i>
                </a>
              </li>
              <li class="nav__item">
                <a href="#intro" class="nav__link c_blue">
                  <i class="fa fa-code" aria-hidden="true"></i>
                </a>
              </li>
              <li class="nav__item">
                <a href="#intro" class="nav__link c_yellow">
                  <i class="fa fa-download" aria-hidden="true"></i>
                </a>
              </li>
              <li class="nav__item">
                <a href="#intro" class="nav__link c_green">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </nav>
        </aside>
        <div style="padding-left:4.25em;">
          <header>
            <div class={isRun ? "startHeaderStatus" : "pauseHeaderStatus"}>
              <h4 style="color:#fff;font-weight:600">
                Create UI For TimeDoctor App
              </h4>
              <strong style="color:#fff;font-weight:600">AloVoip</strong>
              <h4 style="color: rgb(255, 255, 255);font-weight: 600;font-size: 30px;position: absolute;top: 11px;right: 136px;">
                {timer}
              </h4>
            </div>
            <div style="height:50px;line-height: 50px;;background:#fff;box-shadow: 0px 1px 8px #00000052;">
              <p style="color:#000;position: absolute;right: 135px;">
                Worked Today: <strong style="font-weight:600">3h 16m</strong>
              </p>
            </div>
            <button
              onclick={toggleStart}
              type="button"
              class={
                isRun ? "buttonStartHeaderStatus" : "buttonPauseHeaderStatus"
              }
            >
              <i
                class={isRun ? "fa fa-pause" : "fa fa-play"}
                aria-hidden="true"
              ></i>
            </button>
          </header>
          <main style="padding:15px;">{vnode.children}</main>
        </div>
      </div>
    ),
  };
}
