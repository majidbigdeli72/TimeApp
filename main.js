const {
  app,
  BrowserWindow,
  Menu,
  Tray,
  nativeImage,
  ipcMain,
} = require("electron");
const path = require("path");
const fs = require("fs");
const logPath = require("./server/const/sqllight").logFile;
const { run } = require("./server/run/run");
const { createTabels } = require("./server/database/sqllight");
let win = null;
async function createWindow() {
  if (!fs.existsSync(logPath)) {
    fs.mkdirSync(logPath);
  }

  let onIcon = path.join(__dirname, "/content/image/on.png");
  let offIcon = path.join(__dirname, "/content/image/off.png");
  let outIcon = path.join(__dirname, "/content/image/out.png");
  let mainfile = path.join(__dirname, "index.html");

  win = new BrowserWindow({
    width: 800,
    height: 600,
    resizable: false,
    icon: onIcon,
    webPreferences: {
      nodeIntegration: true,
      preload: __dirname + "/preload.js",
    },
  });

  win.loadFile(mainfile);

  var appIcon = new Tray(onIcon);

  var contextMenu = Menu.buildFromTemplate([
    {
      label: "Show App",
      click: function() {
        win.show();
      },
    },
    {
      label: "Quit",
      click: function() {
        app.isQuiting = true;
        app.quit();
      },
      icon: nativeImage.createFromPath(outIcon).resize({ width: 11 }),
    },
  ]);

  appIcon.setContextMenu(contextMenu);

  ipcMain.on("run", async (e, arg) => {

    if (arg == true) {
      console.log(arg)
      await run();
    }
    // win.webContents.send("async-message-replay","ttttttttt");
  });

  appIcon.on("double-click", function() {
    win.show();
  });

  var menu = Menu.buildFromTemplate([
    {
      label: "Menu",
      submenu: [
        { label: "Adjust Notification Value" },
        { label: "CoinMarketCap" },
        {
          label: "Exit",
          click() {
            app.quit();
          },
        },
      ],
    },
  ]);

  Menu.setApplicationMenu(menu);

  win.on("minimize", function(event) {
    event.preventDefault();
    appIcon.setImage(offIcon);
    win.hide();
  });

  win.on("show", function() {
    appIcon.setImage(onIcon);
  });

  win.on("close", function() {
    console.log(global.Title);
    win = null;
  });

  await createTabels();
  win.webContents.openDevTools();
}
const gotTheLock = app.requestSingleInstanceLock();
if (!gotTheLock) {
  app.quit();
} else {
  app.on("second-instance", (event, commandLine, workingDirectory) => {
    // Someone tried to run a second instance, we should focus our window.
    if (win) {
      if (win.isMinimized()) {
        win.show();
      } else {
        win.focus();
      }
    }
  });

  // Create myWindow, load the rest of the app, etc...
  app.whenReady().then(async () => {
    await createWindow();
  });
}
