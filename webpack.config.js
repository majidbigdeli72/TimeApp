module.exports = {
  entry: "./src/index.js",
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      //  {
      //    test: /\.(ttf|eot|woff|woff2)$/,
      //    use: {
      //      loader: "file-loader",
      //      options: {
      //        name: "/content/font/[name].[ext]",
      //      },
      //    },
      //  },
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".html"],
  },
  output: {
    path: __dirname + "/bin",
    publicPath: "/bin",
    filename: "app.js",
  },
  devServer: {
    stats: "errors-only",
    watchContentBase: true,

    //    contentBase: './dist',

    // Parse host and port from env to allow customization.
    //
    // If you use Docker, Vagrant or Cloud9, set
    // host: "0.0.0.0";
    //
    // 0.0.0.0 is available to all network devices
    // unlike default `localhost`.
    host: process.env.HOST, // Defaults to `localhost`
    port: process.env.PORT, // Defaults to 8080
    open: true, // Open the page in browser
  },
};
